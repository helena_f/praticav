package sistemabanco;

import java.util.ArrayList;
import java.util.Scanner;
/**
 *
 * @author Helena
 */
public class GerenciarConta {
    private static ArrayList<Conta> dados = new ArrayList<Conta>();
    Scanner ler = new Scanner(System.in);
    
    public boolean CadastrarConta(){
        System.out.println("Digite o nome do cliente: ");
        String nome = ler.nextLine();
        
        System.out.println("Digite o CPF do cliente: ");
        String cpf = ler.nextLine();
        
        System.out.println("Digite o tipo da conta: ");
        System.out.println("CP - para Conta Poupança");
        System.out.println("CC - para Conta Corrente");
        String tipoC = ler.nextLine();
                
        Conta c = new Conta(nome, cpf, tipoC, 0);
        dados.add(c);
        
        System.out.println(c.toString());
        System.out.println("Conta cadastrada com sucesso");
        return false;
    }
    
    public boolean ListarConta(){
        for(int i = 0; i < dados.size(); i++){
            System.out.println(dados.get(i));
        }
        return false;
    }
    
    public Conta PesquisaConta(){
        Conta resultado = null;
        System.out.println("Busca de Conta");
        System.out.println("Digite o CPF");
        String cpf = ler.nextLine();
        
        for(int i = 0; i < dados.size(); i++){
            Conta atual = dados.get(i);
            
            if(atual.getCpf().equals(cpf)){
                resultado = atual;
                System.out.println(atual);
                break;
            }
            else{
                System.out.println("Conta não encontrada");
            }
        }
        return resultado; 
    }
    
    public Conta DepositarConta(){
        Conta resultado = null;
        System.out.println("Busca de Conta");
        System.out.println("Digite o CPF");
        String cpf = ler.nextLine();
        
        for(int i = 0; i < dados.size(); i++){
            Conta atual = dados.get(i);
            
            if(atual.getCpf().equals(cpf)){
                resultado = atual;
                System.out.println("Digite o valor que deseja depositar: ");
                float valor = ler.nextFloat();
               
                atual.deposita(valor);
                System.out.println(atual.toString());
                System.out.println("Depósito realizado com sucesso");
                break;
            }
            else{
                System.out.println("Conta não encontrada");
            }
        }
        return resultado; 
    }
    
    public Conta SaqueConta(){
        Conta resultado = null;
        System.out.println("Busca de Conta");
        System.out.println("Digite o CPF");
        String cpf = ler.nextLine();
        
        for(int i = 0; i < dados.size(); i++){
            Conta atual = dados.get(i);
            
            if(atual.getCpf().equals(cpf)){
                resultado = atual;
                System.out.println("Digite o valor que deseja sacar: ");
                float valor = ler.nextFloat();

                if(valor > atual.getSaldo()){
                    System.out.println("Valor do saque maior que o saldo!");
                }
                else{
                    atual.saca(valor);
                    System.out.println(atual.toString());
                    System.out.println("Saque realizado com sucesso");
                    break;
                }
               
                
            }
            else{
                System.out.println("Conta não encontrada");
            }
        }
        return resultado; 
    }
    
    public boolean VisualizaSaldo(){
        Conta resultado = null;
        System.out.println("Saldo de Conta");
        System.out.println("Digite o CPF");
        String cpf = ler.nextLine();
        
        for(int i = 0; i < dados.size(); i++){
            Conta atual = dados.get(i);
            
            if(atual.getCpf().equals(cpf)){
                resultado = atual;
                atual.saldo();
                break;
            }
            else{
                System.out.println("Conta não encontrada");
            }
        }
        return false;
    }
}
