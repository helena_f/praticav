package sistemabanco;

import java.util.Scanner;

/**
 *
 * @author Helena
 */
public class SistemaBanco {
    public static void main(String[] args) {
        Scanner ler = new Scanner(System.in);
        
        GerenciarConta gerenciar = new GerenciarConta();
        Menu m = new Menu();
        int op;
        
        do{
            m.Menu();
            op = ler.nextInt();
            switch(op){
                case 1: 
                    gerenciar.CadastrarConta();
                    break;
                case 2: 
                    gerenciar.SaqueConta();
                    break;
                case 3:
                    gerenciar.DepositarConta();
                    break;
                case 4: 
                    gerenciar.VisualizaSaldo();
                    break;
                case 5: 
                    gerenciar.ListarConta();
                    break;
                            
           }
        }while(op!=6);

    }
    
}
