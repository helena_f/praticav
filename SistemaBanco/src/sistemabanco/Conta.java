package sistemabanco;

/**
 *
 * @author Helena
 */
public class Conta {
    private String nome;
    private String cpf;
    private String tipoC;
    private float saldo;

    public Conta(String nome, String cpf, String tipoC, float saldo) {
        this.nome = nome;
        this.cpf = cpf;
        this.tipoC = tipoC;
        this.saldo = saldo;
    }  
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getTipoC() {
        return tipoC;
    }

    public void setTipoC(String tipoC) {
        this.tipoC = tipoC;
    }

    public float getSaldo() {
        
        return saldo;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }

    @Override
    public String toString() {
        return "Conta{" + "nome=" + nome + ", cpf=" + cpf + ", tipoC=" + tipoC + ", saldo=" + saldo + '}';
    }

    void saldo(){
        System.out.println("O Saldo da conta é: "+saldo);
    }
    
    void deposita(float valor){
        float novoSaldo = saldo + valor;
        this.saldo = novoSaldo;
    }
    
    void saca(float valor){
        float novoSaldo = saldo - valor;
        this.saldo = novoSaldo;
    }
}
